<?php

/**
 * @file
 * Contains information about hooks defined in this module.
 */

/**
 * Edit existing Web3Provider definitions.
 *
 * @param array $definitions
 *   List of web3_providers plugin definitions, passed by reference.
 */
function hook_web3_provider_info_alter(array &$definitions) {
  foreach ($definitions as $plugin_id => $plugin_info) {
    if (isset($plugin_info['provider']) && $plugin_info['provider'] == 'mymodule') {
      // Register a new form for plugins provided by mymodule only.
      if (!($definition['forms']['new_form_mode'] ?? NULL)) {
        $definition['forms']['new_form_mode'] = '\Drupal\mymodule\PluginForm\MyModuleForm';
      }
    }
  }
}
