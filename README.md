# Web3 Provider

This module provides base plugin and forms to interact with blockchains from Drupal.

Read more about it [on the module page](https://www.drupal.org/project/web3_provider).

## Credits

[Provider icons created by Eucalyp at Flaticon](https://www.flaticon.com/free-icon/provider_1935113)
