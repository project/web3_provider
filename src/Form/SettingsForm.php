<?php

namespace Drupal\web3_provider\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure our custom settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'web3_provider.settings';

  /**
   * @var \Drupal\web3_provider\Web3ProviderManagerInterface
   */
  protected $pluginManager;

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pluginManager = $container->get('plugin.manager.web3_provider');
    $instance->pluginFormFactory = $container->get('plugin_form.factory');
    $instance->moduleHandler = $container->get('module_handler');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web3_provider_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $config_names = [];
    $config_names[] = static::SETTINGS;
    foreach (array_keys($this->pluginManager->getDefinitions()) as $plugin_id) {
      $config_names[] = 'web3_provider.provider.' . $plugin_id . '.settings';
    }
    return $config_names;
  }

  /**
   * Get a plugin form.
   */
  public function getPluginForm($plugin, $operation) {
    if ($plugin instanceof PluginWithFormsInterface) {
      if ($plugin->hasFormClass($operation)) {
        return $this->pluginFormFactory->createInstance($plugin, $operation);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $providers = $this->pluginManager->getDefinitions();
    $plugins = array_column($providers, 'title', 'id');
    asort($plugins);

    if (empty($plugins)) {
      $form['empty'] = [
        '#type' => 'item',
        '#markup' => $this->t('No plugin found.'),
      ];

      if ($this->moduleHandler->moduleExists('help')) {
        $form['information'] = [
          '#type' => 'link',
          '#title' => $this->t('Read the documentation.'),
          '#url' => Url::fromRoute('help.page', ['name' => 'web3_provider']),
        ];
      } else {
        $form['information'] = [
          '#type' => 'link',
          '#title' => $this->t('Learn how to use this module.'),
          '#url' => Url::fromUri('https://drupal.org/project/web3_provider'),
        ];
      }
      return $form;
    }

    $default_plugin_id = $config->get('default_provider');

    $form['default_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Default provider'),
      '#options' => $plugins,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $default_plugin_id,
      '#required' => TRUE,
    ];

    $form['providers'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Providers'),
      '#default_tab' => '#edit-' . Html::getId($default_plugin_id),
    ];

    $form['#tree'] = TRUE;

    // Build providers plugin form.
    foreach ($providers as $plugin_id => $plugin_info) {
      $form[$plugin_id] = [
        '#type' => 'details',
        '#title' => $plugin_info['title'],
        '#description' => $plugin_info['description'],
        '#group' => 'providers',
      ];

      $plugin_settings = [];
      if ($plugin_config = $this->config('web3_provider.provider.' . $plugin_id . '.settings')) {
        $plugin_settings = $plugin_config->getRawData();
      }
      $provider_plugin = $this->pluginManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($provider_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $form[$plugin_id] = $plugin_form->buildConfigurationForm($form[$plugin_id], $subform_state);
      }
    }

    $form['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug mode'),
      '#description' => $this->t('If enabled, all requests are saved in log.'),
      '#default_value' => $config->get('debug_mode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Save provider configurations.
    foreach (array_keys($this->pluginManager->getDefinitions()) as $plugin_id) {
      $plugin_settings = $form_state->getValues()[$plugin_id] ?? [];
      $provider_plugin = $this->pluginManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($provider_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $plugin_form->validateConfigurationForm($form[$plugin_id], $subform_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('default_provider', (string) $form_state->getValue('default_provider'))
      ->set('debug_mode', (string) $form_state->getValue('debug_mode'))
      ->save();

    // Save provider configurations.
    foreach (array_keys($this->pluginManager->getDefinitions()) as $plugin_id) {
      $plugin_settings = $form_state->getValues()[$plugin_id] ?? [];
      $provider_plugin = $this->pluginManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($provider_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $plugin_form->submitConfigurationForm($form[$plugin_id], $subform_state);
      }

      $plugin_config = $this->configFactory->getEditable('web3_provider.provider.' . $plugin_id . '.settings');
      foreach ($plugin_settings as $key => $value) {
        $plugin_config->set($key, $value);
      }
      $plugin_config->save();
    }

    parent::submitForm($form, $form_state);
  }
}
