<?php

namespace Drupal\web3_provider\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to select a Web3 Provider plugin and update the cache context.
 */
class Web3ProviderSwitcherForm extends FormBase {

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The web3_provider plugin manager.
   *
   * @var \Drupal\web3_provider\Web3ProviderManagerInterface
   */
  protected $pluginManager;

  /**
   * The cache object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    $instance->pluginManager = $container->get('plugin.manager.web3_provider');
    $instance->cache = $container->get('cache.data');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'web3_provider_switcher_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $current_provider = NULL) {
    $plugins = array_column($this->pluginManager->getDefinitions(), 'title', 'id');
    asort($plugins);
    unset($plugins['default']);

    $form['web3_provider'] = [
      '#type' => 'select',
      '#options' => $plugins,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $current_provider,
      '#ajax' => ['callback' => [$this, 'ajaxRefresh']],
      '#required' => TRUE,
    ];

    $form['#attached']['library'][] = 'core/drupal.ajax';

    return $form;
  }

  /**
   * Ajax callback.
   */
  public function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $this->submitForm($form, $form_state);

    // Refresh the page.
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand(Url::fromRoute('<current>')->toString()));
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Nothing to validate.
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Set cache context.
    $cid = 'web3_provider:' . $this->account->id();
    $this->cache->set($cid, $form_state->getValue('web3_provider'));
  }
}
