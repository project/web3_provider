<?php

namespace Drupal\web3_provider\PluginForm;

/**
 * Defines the common interface for all Web3Provider plugin forms.
 *
 * @see \Drupal\web3_provider\Web3ProviderManager
 * @see \Drupal\web3_provider\Annotation\Web3Provider
 * @see plugin_api
 */
interface Web3ProviderPluginFormInterface {

  /**
   * Get the form mode ID to build a unique form ID.
   *
   * @return string
   *   The form mode ID.
   */
  public function getFormModeId(): string;
}
