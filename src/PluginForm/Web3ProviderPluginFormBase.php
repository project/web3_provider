<?php

namespace Drupal\web3_provider\PluginForm;

use Drupal\web3_provider\PluginForm\Web3ProviderPluginFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A base form class for Web3Provider plugin configuration.
 */
abstract class Web3ProviderPluginFormBase extends PluginFormBase implements Web3ProviderPluginFormInterface {

  use StringTranslationTrait;

  /**
   * Return the plugin instance.
   *
   * @return \Drupal\web3_provider\Web3ProviderPluginInterface
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    $form_id = [];
    $form_id[] = 'web3_provider';
    if ($plugin_id = $this->getPlugin()->getPluginId() ?? NULL) {
      $form_id[] = 'plugin';
      $form_id[] = $plugin_id;
    }

    // Make a unique ID the form mode.
    $form_id[] = $this->getFormModeId();

    $form_id[] = 'form';

    return implode('_', $form_id);
  }
}
