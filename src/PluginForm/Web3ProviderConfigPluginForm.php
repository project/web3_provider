<?php

namespace Drupal\web3_provider\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\web3_provider\PluginForm\Web3ProviderPluginFormBase;

/**
 * A base form class for Web3Provider plugin configuration.
 */
class Web3ProviderConfigPluginForm extends Web3ProviderPluginFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormModeId(): string {
    return 'config';
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $plugin = $this->getPlugin();
    if (method_exists($plugin, 'buildConfigurationForm')) {
      $form = array_merge($form, $plugin->buildConfigurationForm($form, $form_state));
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin = $this->getPlugin();
    if (method_exists($plugin, 'validateConfigurationForm')) {
      $plugin->validateConfigurationForm($form, $form_state);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->getPlugin()->setConfiguration($form_state->getValues());

    $plugin = $this->getPlugin();
    if (method_exists($plugin, 'submitConfigurationForm')) {
      $plugin->submitConfigurationForm($form, $form_state);
    }
  }
}
