<?php

namespace Drupal\web3_provider\ContextProvider;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\TermInterface;
use Drupal\web3_provider\Web3ProviderManagerInterface;

/**
 * Sets the an Ethereum web3_provider as a context.
 *
 * @group web3_provider
 */
class CurrentProviderContext implements ContextProviderInterface {

  use StringTranslationTrait;

  /**
   * The cache object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The web3_provider plugin manager.
   *
   * @var \Drupal\web3_provider\Web3ProviderManagerInterface
   */
  protected $pluginManager;

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Constructs a new CurrentNetworkContext.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The route match handler.
   * @param \Drupal\web3_provider\Web3ProviderManagerInterface $web3_provider_manager
   *   The web3_provider manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(
    CacheBackendInterface $cache,
    RouteMatchInterface $current_route_match,
    Web3ProviderManagerInterface $web3_provider_manager,
    AccountInterface $current_user
  ) {
    $this->cache = $cache;
    $this->routeMatch = $current_route_match;
    $this->pluginManager = $web3_provider_manager;
    $this->account = $current_user;
  }

  /**
   * Return the plugin context definition.
   *
   * @return \Drupal\Core\Plugin\Context\ContextDefinition
   *   The definition.
   */
  public static function getContextDefinition() {
    return ContextDefinition::create('string')
      ->setLabel(t("Active Web3 Provider"))
      ->setRequired(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $result = [];

    foreach ($unqualified_context_ids as $context_id) {
      switch ($context_id) {
        case 'web3_provider':
          $plugin_id = NULL;
          $context_definition = self::getContextDefinition();

          // Get web3_provider from route.
          $plugin_id = $this->routeMatch->getParameter('web3_provider');

          // Get web3_provider from cache by user.
          $cid = 'web3_provider:' . $this->account->id();
          if (!$plugin_id && ($cache = $this->cache->get($cid))) {
            $plugin_id = $cache->data;
          }

          if (!$plugin_id) {
            $plugin_id = $this->pluginManager->getFallbackPluginId('default');
          }

          $cacheability = new CacheableMetadata();
          $cacheability->setCacheContexts(['session', 'user']);

          $context = new Context($context_definition, $plugin_id);
          $context->addCacheableDependency($cacheability);
          $result[$context_id] = $context;
          break;
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    return $this->getRuntimeContexts(['web3_provider']);
  }
}
