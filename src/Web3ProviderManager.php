<?php

namespace Drupal\web3_provider;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\web3_provider\Plugin\Web3Provider\Web3ProviderPluginInterface;
use Drupal\web3_provider\Annotation\Web3Provider as Web3ProviderAnnotation;
use Drupal\web3_provider\PluginForm\Web3ProviderConfigPluginForm;
use \Traversable as Traversable;

/**
 * Provides a Web3Provider plugin manager.
 *
 * @see plugin_api
 */
class Web3ProviderManager extends DefaultPluginManager implements Web3ProviderManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Web3Provider',
      $namespaces,
      $module_handler,
      Web3ProviderPluginInterface::class,
      Web3ProviderAnnotation::class
    );
    $this->alterInfo('web3_provider_info');
    $this->setCacheBackend($cache_backend, 'web3_provider_info_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginIds() {
    return array_keys($this->getDefinitions());
  }

  /**
   * {@inheritDoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    if (!isset($definition['forms']['edit'])) {
      $definition['forms']['edit'] = Web3ProviderConfigPluginForm::class;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'default';
  }
}
