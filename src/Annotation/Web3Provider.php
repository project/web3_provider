<?php

namespace Drupal\web3_provider\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a web3_provider annotation object.
 *
 * Plugin Namespace: Plugin\Web3Provider
 *
 * @see plugin_api
 *
 * @Annotation
 */
class Web3Provider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * List of custom plugin forms.
   *
   * @see hook_web3_provider_info_alter();
   * @see \Drupal\Core\Plugin\PluginWithFormTrait;
   * @see \Drupal\web3_provider\PluginForm\Web3ProviderConfigFormBase;
   *
   * @var array
   */
  public $forms;

  /*
   * An array of context definitions describing the context used by the plugin.
   *
   * The array is keyed by context names.
   *
   * @var \Drupal\Core\Annotation\ContextDefinition[]
   */
  public $context_definitions = [];
}
