<?php

namespace Drupal\web3_provider;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;

/**
 * Defines the common interface for all Web3Provider manager classes.
 *
 * @see \Drupal\web3_provider\Web3ProviderManager
 * @see plugin_api
 */
interface Web3ProviderManagerInterface extends FallbackPluginManagerInterface {
}
