<?php

namespace Drupal\web3_provider\Plugin\Web3Provider;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\ContextAwarePluginTrait;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\web3_provider\Plugin\Web3Provider\Web3ProviderPluginInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Web3Provider plugins.
 */
abstract class Web3ProviderPluginBase extends PluginBase implements Web3ProviderPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use PluginWithFormsTrait;
  use ContextAwarePluginTrait;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Key service.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * The plugin context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.web3_provider');
    $instance->httpClient = $container->get('http_client');
    $instance->eventDispatcher = $container->get('event_dispatcher');
    $instance->configFactory = $container->get('config.factory');
    $instance->keyRepository = $container->get('key.repository');
    $instance->contextHandler = $container->get('context.handler');
    $instance->contextRepository = $container->get('context.repository');

    // Populate available contexts.
    if (!isset($configuration['no_reset_context'])) {
      $instance->setDefinedContextValues();
    }

    // Populate with default values.
    $instance->setConfiguration($configuration);

    return $instance;
  }

  /**
   * Set values for the defined contexts of this plugin.
   *
   * @see https://www.hashbangcode.com/article/drupal-10-creating-context-aware-plugins
   */
  protected function setDefinedContextValues() {
    $available_contexts = $this->contextRepository->getAvailableContexts();
    $available_runtime_contexts = $this->contextRepository->getRuntimeContexts(array_keys($available_contexts));
    $plugin_context_definitions = $this->getContextDefinitions();
    foreach ($plugin_context_definitions as $name => $plugin_context_definition) {
      $matches = $this->contextHandler->getMatchingContexts($available_runtime_contexts, $plugin_context_definition);
      $matching_context = reset($matches);
      $this->setContextValue($name, $matching_context->getContextValue());
    }
  }

  /*************************************************************************
   * Configurable plugin methods
   *************************************************************************/

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $plugin_id = $this->getPluginId();
    $config_key = 'web3_provider.provider.' . $plugin_id . '.settings';
    $plugin_config = $this->configFactory->get($config_key);

    return $plugin_config ? $plugin_config->getRawData() : [];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $default_config = $this->defaultConfiguration();
    $this->configuration = $default_config + $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /*************************************************************************
   * Required plugin interface methods.
   *************************************************************************/

  /**
   * {@inheritDoc}
   */
  public function getEndpointUri(): string {
    if ($key_id = $this->configuration['web3_http_provider'] ?? NULL) {
      $key = $this->keyRepository->getKey($key_id);
      $secret = $key->getKeyValues();
      return reset($secret) ?? NULL;
    }

    // Default Ganache CLI endpoint.
    return 'http://localhost:8545';
  }

  /**
   * {@inheritDoc}
   */
  public function requests(array $requests): array {
    $request_options = [];

    // @todo What headers would be useful?
    $request_options[RequestOptions::HEADERS] = [];

    // Process batch request.
    $i = 0;
    foreach (array_filter($requests) as $i => $request) {
      $method = key($request);
      $parameters = $request[$method];
      $batch[] = [
        'jsonrpc' => '2.0',
        'method' => $method,
        'params' => $parameters,
        'id' => $i++,
      ];
    };

    // Do not send one request in batch to iImprove log info.
    // (e.g. look at `Recent Requests` in Alchemy dashboard).
    if ($single_transaction = count($batch) == 1) {
      $batch = reset($batch);
    }

    try {
      $request_method = 'POST';
      $request_uri = $this->getEndpointUri();
      $request_options[RequestOptions::BODY] = Json::encode($batch);
      $response = $this->httpClient->request($request_method, $request_uri, $request_options);
      $results = Json::decode($response->getBody()->getContents());
      // Always return an array, even for single transaction.
      $results = $single_transaction ? [$results] : $results;
    } catch (\Exception $e) {
      // Log error requests.
      if ($this->configFactory->get('web3_provider.settings')->get('debug_mode') == '1') {
        $this->logger->error($this->t('@method @uri:<br><pre>@body</pre>', [
          '@method' => $request_method,
          '@uri' => $request_uri,
          '@body' => $request_options[RequestOptions::BODY],
        ]));
      }

      throw $e;
    }

    // Log successful requests.
    if ($this->configFactory->get('web3_provider.settings')->get('debug_mode') == '1') {
      $this->logger->notice($this->t('@method @uri:<br><pre>@body</pre><br><pre>@results</pre>', [
        '@method' => $request_method,
        '@uri' => $request_uri,
        '@body' => $request_options[RequestOptions::BODY],
        '@results' => Json::encode($results),
      ]));
    }

    return $results;
  }
}
