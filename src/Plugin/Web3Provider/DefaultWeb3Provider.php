<?php

namespace Drupal\web3_provider\Plugin\Web3Provider;

use Drupal\web3_provider\Plugin\Web3Provider\Web3ProviderPluginBase;

/**
 * Defines a default web3_provider plugin to communicate with ETH nodes.
 *
 * @Web3Provider(
 *   id = "default",
 *   title = @Translation("Default Web3 Provider"),
 *   description = @Translation("Provides a default provider without any functionality (e.g. used for fallback only).")
 * )
 */
class DefaultWeb3Provider extends Web3ProviderPluginBase {
}
