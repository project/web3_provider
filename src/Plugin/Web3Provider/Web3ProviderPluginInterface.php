<?php

namespace Drupal\web3_provider\Plugin\Web3Provider;

use Drupal\Component\Plugin\ContextAwarePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;

/**
 * Defines the common interface for all Web3Provider plugin classes.
 *
 * @see \Drupal\web3_provider\Web3ProviderManager
 * @see \Drupal\web3_provider\Annotation\Web3Provider
 * @see plugin_api
 */
interface Web3ProviderPluginInterface extends PluginInspectionInterface, PluginWithFormsInterface, ContextAwarePluginInterface {

  /**
   * Get the provider API endpoint.
   *
   * @return string
   *   The URL where to send our requests.
   */
  public function getEndpointUri(): string;

  /**
   * Perform a list of queries to the provider.
   *
   * @param array $requests
   *   A given list of request to make. Each entry has a key and an array value
   *   such as: ```$requests = [[$jsonrpc_method => $parameters]];``` where
   *   `$jsonrpc_evm_method` is a given method of the Ethereum Virtual Machine
   *   (e.g. 'eth_estimategas') and `$parameters` are the arguments of the call.
   *
   * @return array
   *   The decoded JSON results.
   *
   * @throws \Exception
   *   Error communicating with the remote service.
   *
   * @see https://ethereum.org/en/developers/docs/apis/json-rpc/
   */
  public function requests(array $requests): array;
}
