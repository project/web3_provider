<?php

namespace Drupal\web3_provider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\web3_provider\Form\Web3ProviderSwitcherForm;

/**
 * Provides a block to test caching.
 *
 * @Block(
 *   id = "web3_provider_switcher_block",
 *   admin_label = @Translation("Web3 Provider switcher"),
 *   category = @Translation("Web3 Provider"),
 *   context_definitions = {
 *     "web3_provider" = @ContextDefinition("string", label = @Translation("Web3 Provider"))
 *   }
 * )
 */
class Web3ProviderSwitcherBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $plugin_id = $this->getContextValue('web3_provider');
    return \Drupal::formBuilder()->getForm(Web3ProviderSwitcherForm::class, $plugin_id);
  }
}
